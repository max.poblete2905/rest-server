const express = require('express')
const cors = require('cors');
const { dbConnection } = require('../database/config');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.userPath = '/api/user'

        //conexion base de datos 
        this.conectarDB();

        // middlewares
        this.middleweres();

        // rutas de mi app
        this.routes();
    }

    async conectarDB() {

        /** aqui se pueden definir diferentes conecciones como 
        para distintos ambientes EJ: PROD, DEV, TEST, LOCAL */
        await dbConnection();
    }

    middleweres() {
        // habilitar cors
        this.app.use(cors());

        // lectura y parseo del body
        this.app.use(express.json());

        // leer carpeta public
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use(this.userPath, require('../routes/user'))
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Example app listening at http://localhost:${this.port}`)
        })
    }
}

module.exports = Server;
