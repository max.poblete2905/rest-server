const { response } = require('express');
const User = require('../model/user');
const bcryptjs = require('bcryptjs');

const userGet = (req, res = response) => {
    const { q, nombre, apikey } = req.query;
    res.json({
        msg: 'get api -- desde controller',
        q,
        nombre,
        apikey,
    });
}

const userPut = (req, res) => {

    const id = req.params.id;
    res.status(400).json({
        msg: 'put api -- controller',
        id,
    });
}

const userPost = async (req, res = response) => {


    const { nombre, correo, password, rol } = req.body;
    const user = new User({ nombre, correo, password, rol });

    // verificar si el correo existe

    // emcriptar la passwors 
    const salt = bcryptjs.genSaltSync(10);
    user.password = bcryptjs.hashSync(password, salt);

    //guardar en db

    await user.save();

    res.json({
        user
    });
}

const userDelete = (req, res) => {
    res.json({
        msg: 'delete api -- controller'
    });
}

const userPatch = (req, res) => {
    res.json({
        msg: 'patch api --controller'
    });
}

module.exports = {
    userGet,
    userPut,
    userPost,
    userDelete,
    userPatch,
}
