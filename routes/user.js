const { Router } = require('express');
const { check } = require('express-validator');
const { userGet, userPut, userPost, userDelete, userPatch } = require('../controllers/user');
const { esRolValido, esCorreoValido } = require('../helper/db-validators');
const { validarCampos } = require('../middlewares/validarCampos');

const router = Router();

router.get('/', userGet)

router.put('/:id', userPut)

router.post('/', [
    check('nombre', 'el nombre no es valido').not().isEmpty(),
    check('password', 'el password debe contener 6 letras').isLength({ min: 6 }),
    check('correo', 'el correo es obligatorio').isEmail(),
    check('correo').custom( esCorreoValido ),
    check('rol').custom( esRolValido ),
    validarCampos,
], userPost)

router.delete('/', userDelete)

router.patch('/', userPatch)

module.exports = router;