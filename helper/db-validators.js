const Rol = require('../model/role');
const User = require('../model/user')

const esRolValido = async (rol = '') => {
    const existeRol = await Rol.findOne({ rol });
    if (!existeRol) {
        throw new Error(`el rol ${rol} no esta registrado en la base de datos `)
    }
}

const esCorreoValido = async (correo = '') => {
    const existeCorreo = await User.findOne({ correo });
    if (existeCorreo) {
        throw new Error(`el correo ${correo} ya esta registrado`)
    }
}

module.exports = {
    esRolValido,
    esCorreoValido,
}
